# HandsonTable (Js)

1. Vào Demo 1 (Hiển thị dữ liệu với JS dạng dữ liệu tĩnh và API)
2. Tạo file .env với nội dung: REST_API = http://localhost:3000

# HandsonTable (Angular)

Bước 1: Cài đặt **JSON-SERVER** (nếu có rồi thì bỏ qua)
Gõ lệnh trong Terminar: **npm install -g json-server**

Bước 2: Gõ lệnh: **json-server --watch ten_file.json** để chạy Server

**Đọc thêm tại LINK bên dưới**

[JSON-SERVER](https://github.com/typicode/json-server#getting-started)

### Tham khảo thêm HandsonTable tại link bên dưới

[HandsonTable](https://handsontable.com/)
