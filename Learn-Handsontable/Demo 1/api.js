import Handsontable from "handsontable/base";

import { isArabicDemoEnabled } from "./utils";
import { progressBarRenderer, starRenderer } from "./customrenders";
import {
  registerCellType,
  CheckboxCellType,
  DateCellType,
  DropdownCellType,
  NumericCellType,
} from "handsontable/cellTypes";

import {
  registerPlugin,
  AutoColumnSize,
  ContextMenu,
  DropdownMenu,
  CopyPaste,
  Filters,
  HiddenColumns,
  HiddenRows,
  ManualRowMove,
  MultiColumnSorting,
  UndoRedo,
} from "handsontable/plugins";

import {
  alignHeaders,
  addClassesToRows,
  changeCheckboxCell,
  drawCheckboxInRowHeaders,
} from "./hooksCallbacks";

registerCellType(DateCellType);
registerCellType(DropdownCellType);
registerCellType(CheckboxCellType);
registerCellType(NumericCellType);

registerPlugin(AutoColumnSize);
registerPlugin(ContextMenu);
registerPlugin(DropdownMenu);
registerPlugin(Filters);
registerPlugin(HiddenColumns);
registerPlugin(HiddenRows);
registerPlugin(MultiColumnSorting);

const container = document.querySelector("#example2");
const notification = document.querySelector("p.notification");

const handsonTable = new Handsontable(container, {
  data: getData(),
  rowHeaders: true,
  startRows: 8,
  startCols: 6,
  height: "auto",
  dropdownMenu: true, // Mở menu ngay ở phần thead trong table
  contextMenu: true,
  multiColumnSorting: true,
  filters: true,
  rowHeaders: true,
  manualRowMove: true,
  copyPaste: {
    copyColumnHeaders: true,
    copyColumnGroupHeaders: true,
    copyColumnHeadersOnly: true,
  },
  outsideClickDeselects: false, // Phần này mới selected được
  licenseKey: "non-commercial-and-evaluation",
});

handsonTable.getPlugin("copyPaste");

const autoSave = document.querySelector("#autosave");

handsonTable.updateSettings({
  afterChange: function (change, source) {
    if (source === "loadData") {
      return; //don't save this change
    }

    if (!autoSave.checked) {
      return;
    }

    console.log({ change });

    fetch(`${process.env.REST_API}/data`, {
      method: "POST",
      body: JSON.stringify({ data: change }),
    })
      .then((response) => {
        notification.innerText = `Autosaved (${change.length} cell${change.length > 1 ? "s" : ""})`;
        console.log("The POST request is only used here for the demo purposes");
      })
      .catch((error) => console.log(err));
  },
});

function getData() {
  const url = `${process.env.REST_API}/data`;

  fetch(url)
    .then((res) => res.json())
    .then((res) => {
      handsonTable.loadData(res.data);
    })
    .catch((error) => console.log(error));

  notification.innerHTML = "Lấy dữ liệu thành công";
}

const btnSave = document.querySelector(".btn");

btnSave.addEventListener("click", () => {
  // save all cell's data
  fetch(`${process.env.REST_API}/data`, {
    method: "POST",
    mode: "no-cors",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ data: handsonTable.getData() }),
  }).then((response) => {
    exampleConsole.innerText = "Data saved";
    console.log("The POST request is only used here for the demo purposes");
  });
});

autosave.addEventListener("click", () => {
  if (autosave.checked) {
    notification.innerText = "Changes will be autosaved";
  } else {
    notification.innerText = "Changes will not be autosaved";
  }
});

const createButton = document.querySelector(".btn-create");

createButton.addEventListener("click", () => {
  const col = handsonTable.countRows();
  handsonTable.alter("insert_row_above", col, 1);
});
