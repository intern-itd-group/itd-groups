import Handsontable from "handsontable/base";
import { generateExampleData, isArabicDemoEnabled } from "./utils";
import { progressBarRenderer, starRenderer } from "./customrenders";
import {
  registerCellType,
  CheckboxCellType,
  DateCellType,
  DropdownCellType,
  NumericCellType,
} from "handsontable/cellTypes";

import {
  registerPlugin,
  AutoColumnSize,
  ContextMenu,
  DropdownMenu,
  CopyPaste,
  Filters,
  HiddenColumns,
  HiddenRows,
  ManualRowMove,
  MultiColumnSorting,
  UndoRedo,
} from "handsontable/plugins";

import {
  alignHeaders,
  addClassesToRows,
  changeCheckboxCell,
  drawCheckboxInRowHeaders,
} from "./hooksCallbacks";

const container = document.querySelector("#example1");

registerCellType(DateCellType);
registerCellType(DropdownCellType);
registerCellType(CheckboxCellType);
registerCellType(NumericCellType);

registerPlugin(AutoColumnSize);
registerPlugin(ContextMenu);
registerPlugin(DropdownMenu);
registerPlugin(Filters);
registerPlugin(HiddenColumns);
registerPlugin(HiddenRows);
registerPlugin(MultiColumnSorting);

const hot = new Handsontable(container, {
  data: generateExampleData(),
  rowHeaders: true,
  colHeaders: [
    "Company name",
    "Name",
    "Sell date",
    "In stock",
    "Qty",
    "Progress",
    "Rating",
    "Order ID",
    "Country",
  ],
  columns: [
    { data: 1, type: "text" },
    { data: 3, type: "text" },
    {
      data: 4,
      type: "date",
      allowInvalid: false,
      dateFormat: isArabicDemoEnabled() ? "M/D/YYYY" : "DD/MM/YYYY",
    },
    {
      data: 6,
      type: "checkbox",
      className: "htCenter",
    },
    {
      data: 7,
      type: "numeric",
    },
    {
      data: 8,
      renderer: progressBarRenderer,
      readOnly: true,
      className: "htMiddle",
    },
    {
      data: 9,
      renderer: starRenderer,
      readOnly: true,
      className: "star htCenter",
    },
    { data: 5, type: "text" },
    { data: 2, type: "text" },
  ],
  height: "auto",
  dropdownMenu: true, // Mở menu ngay ở phần thead trong table
  contextMenu: true,
  multiColumnSorting: true,
  filters: true,
  rowHeaders: true,
  manualRowMove: true,
  licenseKey: "non-commercial-and-evaluation",
});
