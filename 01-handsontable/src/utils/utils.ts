export function isArabicDemoEnabled() {
  const urlParams = new URLSearchParams(location.search.replace(/^\?/, ''));

  return urlParams.get('arabicExample') === '1';
}
