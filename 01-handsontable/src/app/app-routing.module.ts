import { HandsontableComponent } from './handsontable/handsontable.component';
import { AppComponent } from './app.component';
import { TableBasicComponent } from './table-basic/table-basic.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'table-basic', component: TableBasicComponent },
  { path: 'handsontable', component: HandsontableComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
