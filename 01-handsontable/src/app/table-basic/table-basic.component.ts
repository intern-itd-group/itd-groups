import { ApiService } from './../Services/api.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-table-basic',
  templateUrl: './table-basic.component.html',
  styleUrls: ['./table-basic.component.scss'],
})
export class TableBasicComponent implements OnInit {
  users: any[] = [];

  constructor(private apiService: ApiService) {}

  getUserList(): any {
    this.apiService.getUser().subscribe({
      next: (res) => {
        this.users = res;
      },
      error: (err) => {
        console.log(err);
      },
    });
  }

  ngOnInit(): void {
    this.getUserList();
  }
}
