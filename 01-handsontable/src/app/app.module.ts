import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HandsontableComponent } from './handsontable/handsontable.component';
import { registerAllModules } from 'handsontable/registry';
import { HotTableModule } from '@handsontable/angular';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  registerCellType, // cell types' registering function
  NumericCellType,
} from 'handsontable/cellTypes';
import {
  registerPlugin, // plugins' registering function
  UndoRedo,
} from 'handsontable/plugins';
import { FormfieldComponent } from './formfield/formfield.component';
import { TableBasicComponent } from './table-basic/table-basic.component';
import { NavComponent } from './nav/nav.component';
// register Handsontable's modules
registerAllModules();

@NgModule({
  declarations: [
    AppComponent,
    HandsontableComponent,
    FormfieldComponent,
    TableBasicComponent,
    NavComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HotTableModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
