import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-formfield',
  templateUrl: './formfield.component.html',
  styleUrls: ['./formfield.component.scss'],
})
export class FormfieldComponent implements OnInit {
  public formDataUser: FormGroup = this.formBuilder.group({
    fullName: ['', Validators.required],
    address: ['', Validators.required],
    age: ['', Validators.required],
    phone: ['', Validators.required],
    date: ['', Validators.required],
  });

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {}

  onSubmit(): void {
    console.log('Form', this.formDataUser.value);
  }
}
