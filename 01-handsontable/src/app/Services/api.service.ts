import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root',
})
export class ApiService {
  REST_API_SERVER = 'http://localhost:3000/users';
  constructor(private http: HttpClient) {}

  postUser(data: any) {
    const url = `${this.REST_API_SERVER}`;
    return this.http.post<any>(url, data);
  }

  getUser() {
    const url = `${this.REST_API_SERVER}`;
    return this.http.get<any>(url);
  }
}
