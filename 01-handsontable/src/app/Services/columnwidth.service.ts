import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ColumnwidthService {
  REST_API_SERVER = 'http://localhost:3000/widthColumns';
  constructor(private http: HttpClient) {}

  getColumnWidths() {
    const url = `${this.REST_API_SERVER}`;
    return this.http.get<any>(url);
  }
}
