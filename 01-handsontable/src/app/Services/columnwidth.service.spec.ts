import { TestBed } from '@angular/core/testing';

import { ColumnwidthService } from './columnwidth.service';

describe('ColumnwidthService', () => {
  let service: ColumnwidthService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ColumnwidthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
