import { ColumnwidthService } from './../Services/columnwidth.service';
import { ApiService } from './../Services/api.service';
import { Component, OnInit, OnChanges } from '@angular/core';
import { HotTableRegisterer } from '@handsontable/angular';
import Handsontable from 'handsontable/base';
import { ContextMenu } from 'handsontable/plugins/contextMenu';
import { isArabicDemoEnabled } from 'src/utils/utils';

@Component({
  selector: 'app-handsontable',
  templateUrl: './handsontable.component.html',
  styleUrls: ['./handsontable.component.scss'],
})
export class HandsontableComponent implements OnInit {
  dataset: any[] = [];
  columnWidths: any[] = [];
  constructor(
    private apiService: ApiService,
    private columnWidthService: ColumnwidthService
  ) {}

  public hotSettings: Handsontable.GridSettings = {
    colHeaders: ['Id', 'Full Name', 'Address', 'Age', 'Phone', 'Date Of Birth'],
    columns: [
      {
        readOnly: true,
        data: 'id',
      },
      {
        data: 'fullName',
      },
      {
        data: 'address',
      },
      {
        data: 'age',
      },
      {
        data: 'phone',
      },
      {
        data: 'dateOfBirth',
        type: 'date',
        allowInvalid: false,
        dateFormat: isArabicDemoEnabled() ? 'M/D/YYYY' : 'DD/MM/YYYY',
      },
    ],
    height: 'auto',
    licenseKey: 'non-commercial-and-evaluation',
    dropdownMenu: true,
    filters: true,
    manualColumnResize: true,
  };
  getUserList() {
    this.apiService.getUser().subscribe({
      next: (res) => {
        this.dataset = res;
      },
      error: (err) => {
        console.log(err);
      },
    });
  }

  getColumnWidthList() {
    this.columnWidthService.getColumnWidths().subscribe({
      next: (res) => {
        this.columnWidths = res;
      },
      error: (err) => {
        console.log(err);
      },
    });
  }

  ngOnInit(): void {
    this.getUserList();
    this.getColumnWidthList();
  }
}
