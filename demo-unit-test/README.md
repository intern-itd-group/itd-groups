# Testing component

**configureTestingModule**

- Là phương thức của class TestBed và chuyển đổi một đối tượng thành tham số

- Tham số cung cấp cho configureTestingModule khi khởi tạo module là những metadata cần thiết cho một module như imports, providers, declarations

**beforeEach**

Tạo mới một module kiểm thử trước khi thực hiện các test case liên quan đến component trong hàm

> beforeEach(async(() => {
> TestBed.configureTestingModule({
> imports: [RouterTestingModule],
> declarations: [AppComponent],
> }).compileComponents();
> }));
