import { MessageModule } from './messages/message.module';
import { ModelModule } from './model/model.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FirstComponentComponent } from './first-component/first-component.component';

@NgModule({
  declarations: [AppComponent, FirstComponentComponent],
  imports: [BrowserModule, AppRoutingModule, ModelModule, MessageModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
